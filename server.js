const http = require('http');
/*
http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Hello World!');
    res.end();
}).listen(8080);
*/

const multer = require('multer');
const cors = require('cors')
const https = require('https');
const fs = require('fs');
const port = 8080;
const serversetupport = 65000;
const upload = multer({ dest: __dirname + '/public/uploads/images' });
const uploadreg = multer({ dest: __dirname + '/responses' });
const uploadconsent = multer({ dest: __dirname + '/consents' });


const express = require('express');
const httpport = 8080;
console.log("Starting server");
app = express();
app.use(express.static('public'));
var httpserver = http.createServer(options, app);

httpserver.listen(httpport, () => {
    console.log("server starting on port : " + httpport)
});

var key = fs.readFileSync('keys/private.key');
var cert = fs.readFileSync('keys/certificate.crt');

var options = {
    key: key,
    cert: cert
};


app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "https://electoralregistration.org"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.post('/upload', upload.single('photo'), (req, res) => {
    if (req.file) {
        res.json(req.file);
    }
    else throw 'error';
});

app.post('/uploadregistration', uploadreg.single('results'), (req, res) => {
    if (req.file) {
        res.json(req.file);
    }
    else throw 'error';
});

app.use(express.urlencoded({
    extended: true
}))

app.post('/eligibleturnout/PISconsent', uploadconsent.any('q5_test'), (req, res) => {
    console.log("Post of consent form submitted");
    file = "consents/" + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + ".txt";
    fs.writeFile(Path = file, Data = req.body.q5_test, (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    })

    console.log(req.body.q5_test);
    res.redirect('https://mozi.fyi/eligibleturnout/videotutorial.html');
});

var server = https.createServer(options, app);
server.listen(port, () => {
    console.log("server starting on port : " + port)
});

/*

class GameServer {
    constructor(newport, newroom) {
        this.port = newport;
        this.room = newroom;
    }
}

var gs = [];

ss = express();
minPort = 49000;
currentPort = minPort;

ss.get('/', function (req, res) {
    console.log(req.url);
    console.log();
    roomRequest = req.url.split("=")[1];
    console.log('Current port is: ' + currentPort);
    console.log('Current room is: ' + roomRequest);
    roomtaken = false;
    for (var i = 0; i < gs.length; i++) {
        if (gs[i].room == roomRequest) {
            roomtaken = true;
        }
    }
    if (!roomtaken) {
        temmpgs = new GameServer(newport = currentPort, newroom = roomRequest);
        gs.push(temmpgs);
        console.log("GS length is: " + gs.length);
        console.log("Creating room " + gs[gs.length - 1].room + " with port " + gs[gs.length - 1].port);
        currentPort++;
        res.send('Room created')
    } else {
        console.log("Web request attempted to create existing room with room ID: " + gs[gs.length - 1].room);
        res.send('Room already created with that room ID.')
    }
})

*/


/*
var serverSS = https.createServer(options, ss);


serverSS.listen(serversetupport, () => {
    console.log("Setup server starting on port : " + serversetupport)
});


var child = require('child_process').execFile;
var executablePath = "D:\\Mozi\\GameServer\\GameServer\\bin\\Release\\netcoreapp3.0\\GameServer.exe";;

child(executablePath, function (err, data) {
    if (err) {
        console.error(err);
        return;
    }

    console.log(data.toString());
});



console.log("got this far");
*/