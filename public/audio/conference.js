﻿// Muaz Khan         - www.MuazKhan.com
// MIT License       - www.WebRTC-Experiment.com/licence
// Experiments       - github.com/muaz-khan/WebRTC-Experiment

// This library is known as multi-user connectivity wrapper!
// It handles connectivity tasks to make sure two or more users can interconnect!

var peerlist = [];

var usergiven = false;
var userrequest;
var gamestart = false;
var mutestart=false;
var track;


function setupPanner() {
    for (let i = 0; i < streamlist.length; i++) {
        if (streamlist[i].pannerset == false) {
            RemotePanner(streamlist[i]);
            streamlist[i].pannerset = true;
        };
    };
};

var locInterval = 100;
function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
};
function spatialRegular() {
    userrequest = setInterval(giveUserID, locInterval); 
    sleep(50);
    setInterval(checkGameStart, locInterval); 
    sleep(50);
    setInterval(spatial, locInterval);
    sleep(50);
    setInterval(spatialPlayers, locInterval);  
    sleep(50);
    setInterval(setupPanner, locInterval);
    sleep(50);
    setInterval(getsettings, locInterval)
};


function getsettings() {
    if (mutestart) {
        var url = 'http://127.0.0.1:8084/settings.txt';

        fetch(url)
            .then(function (response) {
                response.text().then(function (text) {
                    settingsText= text;
                });
            });
        if (settingsText == "shutdown") {
            window.close();
        }
        
        //console.log(settingsText);
        track = streamout.getAudioTracks()[0]
        if (settingsText.includes("mute=False")) {
            track.enabled = true;
        }
        if (settingsText.includes("mute=True")) {
            track.enabled = false;
        }
    }
}

function spatial() {
    if (usergiven & gamestart) {
        var url = 'http://127.0.0.1:8084/playerposition.txt';

        fetch(url)
            .then(function (response) {
                response.text().then(function (text) {
                    storedTextSelf = text;
                });
            });
        if (storedTextSelf == "shutdown") {
            window.close();
        }

        
        mul = parseFloat(document.getElementById('multbox').value);
        refdist = parseFloat(document.getElementById('refdistbox').value);
        rolloff = parseFloat(document.getElementById('rolloffbox').value);
        
        var loc = storedTextSelf.split(",");
        var loc2 = loc.map(x => +x);
        window.ctx.listener.positionX.value = loc2[0] * mul;
        window.ctx.listener.positionY.value = loc2[1] * mul;
        window.ctx.listener.positionZ.value = loc2[2] * mul;
        
        var listenerOrientation = principalAxesToOrientation(loc[3], loc[4], loc[5])
        window.ctx.listener.forwardX.setValueAtTime(listenerOrientation.forward.x, window.ctx.currentTime);
        window.ctx.listener.forwardY.setValueAtTime(listenerOrientation.forward.y, window.ctx.currentTime);
        window.ctx.listener.forwardZ.setValueAtTime(listenerOrientation.forward.z, window.ctx.currentTime);
        window.ctx.listener.upX.setValueAtTime(listenerOrientation.up.x, window.ctx.currentTime);
        window.ctx.listener.upY.setValueAtTime(listenerOrientation.up.y, window.ctx.currentTime);
        window.ctx.listener.upZ.setValueAtTime(listenerOrientation.up.z, window.ctx.currentTime);

        window.ctx.listener.refDistance  = refdist * mul;
        window.ctx.listener.rolloffFactor = rolloff;

    //window.ctx.listener.setOrientation(loc2[3], loc2[4], loc2[5]);
    // replace with up vector and forward vector syntax
    }
};
giveUserID = function() {
    if (!usergiven) {
        var url = 'http://127.0.0.1:8084/user' + config.attachStream.id;
        fetch(url)
            .then(function (response) {
                response.text().then(function (text) {
                    userResponse = text;
                });
            });
        if (userResponse == "shutdown") {
            window.close();
        }
        if (userResponse == "success") {
            usergiven = true;
        }
    }
}
var playernumber;
checkGameStart = function () {
    if (!gamestart) {
        var url = 'http://127.0.0.1:8084/playernumbers';
        fetch(url)
            .then(function (response) {
                response.text().then(function (text) {
                    playernumber= parseInt(text);
                });
            });
        if (playernumber == "shutdown") {
            window.close();
        }
        if (playernumber>0) {
            gamestart = true;
        }
        if (playernumber == 0) {
            mutestart = true;
        }
    }
}



function spatialPlayers() {
    if (usergiven & gamestart) {

        var url = 'http://127.0.0.1:8084/otherpositions.txt';
        if (streamlist.length > 0) {
            fetch(url)
                .then(function (response) {
                    response.text().then(function (text) {
                        storedTextPlayer = text;
                    });
                });
            //console.log(window.storedText);
            if (storedTextPlayer == "shutdown") {
                window.close();
            }
            let players = storedTextPlayer.split("\n");
            if (players[players.length - 1] == "") {
                players.pop();
            }

            for (let i = 0; i < players.length; i++) {
                playerparts = players[i].split(",");

                tempidplayer = playerparts[1];
                if (tempidplayer != null) {
                    let playerparts2 = playerparts.map(x => +x);

                    for (let j = 0; j < streamlist.length; j++) {
                        streamlist[j].found = false;

                        if (streamlist[j].id.trim() == tempidplayer.trim()) {
                            // update panner position and orientation for relevant player

                            streamlist[j].found = true;
                            
                            streamlist[j].panner.setPosition(playerparts2[3] * mul,
                                playerparts2[4] * mul,
                                playerparts2[5] * mul);
                            
                     
                            //var pannerOrientation = calculatePannerOrientation({ yaw: playerparts2[6], pitch: playerparts2[7] });
                            var pannerOrientation = calculatePannerOrientation({ yaw: playerparts2[7], pitch: playerparts2[6] });
                            
                                                   
                            streamlist[j].panner.orientationX.setValueAtTime(pannerOrientation.x, window.ctx.currentTime);
                            streamlist[j].panner.orientationY.setValueAtTime(pannerOrientation.y, window.ctx.currentTime);
                            streamlist[j].panner.orientationZ.setValueAtTime(pannerOrientation.z, window.ctx.currentTime);
                            
                            streamlist[j].panner.refDistance = refdist * mul;
                            streamlist[j].panner.rolloffFactor = rolloff;

                            
                            streamlist[j].panner.coneInnerAngle = coneInnerAngle;
                            streamlist[j].panner.coneOuterAngle = coneOuterAngle;
                            streamlist[j].panner.coneOuterGain = coneOuterGain;
                            
                            
                        };
                    };
                }
            };
        }
    }
};


var findOne = function (haystack, arr) {
    return arr.some(function (v) {
        return haystack.indexOf(v) >= 0;
    });
};

function removeDuplicates(data) {
    return data.filter((value, index) => data.indexOf(value) === index);
}

function getIDOne(part, ip) {
    details = part.peer.remoteDescription.sdp;
    details = details.split("a=");
    msid = $.grep(details, function (dt) {
        return dt.indexOf("msid") != -1;
    });
    msid = msid[0].split(" ")[2].split("\n")[0];

    details = $.grep(details, function (dt) {
        return dt.indexOf("candidate:") != -1;
    });


    for (let i = 0; i < details.length; i++) {
        details[i] = details[i].split(" ")[4];
    };
    details = removeDuplicates(details);
    test = findOne(ip, details);
    if (test == true) {
        return msid;
    } else {
        return null;
    }
};

getID = function (ip) {
    for (let i = 0; i < peerlist.length; i++) {
        let tempid = getIDOne(peerlist[i], ip);
        if (tempid != null) {
            return tempid
        }
    };
    return null;
};


function onIceStateChange(pc, event) {
    console.log("here 1, 2, 3");
    if (pc) {
        console.log(`${getName(pc)} ICE state: ${pc.iceConnectionState}`);
        console.log('ICE state change event: ', event);
    }
}


var conference = function(config) {
    var self = {
        userToken: uniqueToken()
    };
    var channels = '--', isbroadcaster;
    var isGetNewRoom = true;
    window.sockets = [];
    var defaultSocket = { };

    function openDefaultSocket(callback) {
        defaultSocket = config.openSocket({
            onmessage: onDefaultSocketResponse,
            callback: function(socket) {
                defaultSocket = socket;
                callback();
            }
        });
    }

    function onDefaultSocketResponse(response) {
        if (response.userToken == self.userToken) return;

        if (isGetNewRoom && response.roomToken && response.broadcaster) config.onRoomFound(response);

        if (response.newParticipant && self.joinedARoom && self.broadcasterid == response.userToken) onNewParticipant(response.newParticipant);

        if (response.userToken && response.joinUser == self.userToken && response.participant &&
            channels.indexOf(response.userToken) == -1) {
            channels += response.userToken + '--';
            openSubSocket({
                isofferer: true,
                channel: response.channel || response.userToken
            });
        }

        // to make sure room is unlisted if owner leaves        
        if (response.left && config.onRoomClosed) {
            config.onRoomClosed(response);
        }
    }

    function openSubSocket(_config) {
        if (!_config.channel) return;
        var socketConfig = {
            channel: _config.channel,
            onmessage: socketResponse,
            onopen: function() {
                if (isofferer && !peer) initPeer();
                window.sockets[window.sockets.length] = socket;
            }
        };

        socketConfig.callback = function(_socket) {
            socket = _socket;
            this.onopen();

            if(_config.callback) {
                _config.callback();
            }
        };

        var socket = config.openSocket(socketConfig),
            isofferer = _config.isofferer,
            gotstream,
            audio = document.createElement('audio'),
            inner = { },
            peer;

        var peerConfig = {
            attachStream: config.attachStream,
            onICE: function (candidate) {
                console.debug("Invoking onICE with userToken: " + self.userToken + " and Candidate: " + candidate.candidate);
                socket.send({
                    userToken: self.userToken,
                    candidate: {
                        sdpMLineIndex: candidate.sdpMLineIndex,
                        candidate: JSON.stringify(candidate.candidate)
                    }
                });
            },
            onRemoteStream: function(stream) {
                if (!stream) return;

                audio.srcObject = stream;

                _config.stream = stream;
                onRemoteStreamStartsFlowing();
            },
            onRemoteStreamEnded: function(stream) {
                if (config.onRemoteStreamEnded)
                    config.onRemoteStreamEnded(stream, audio);
            }
        };

        function initPeer(offerSDP) {
            if (!offerSDP) {
                peerConfig.onOfferSDP = sendsdp;
            } else {
                peerConfig.offerSDP = offerSDP;
                peerConfig.onAnswerSDP = sendsdp;
            }

            peer = RTCPeerConnection(peerConfig);

            // new code
            peer.oniceconnectionstatechange = e => onIceStateChange(peer, e);
            peerlist.push(peer);
        }
        
        function afterRemoteStreamStartedFlowing() {
            gotstream = true;

            if (config.onRemoteStream)
                config.onRemoteStream({
                    audio: audio,
                    stream: _config.stream
                });

            if (isbroadcaster && channels.split('--').length > 3) {
                /* broadcasting newly connected participant for video-conferencing! */
                defaultSocket.send({
                    newParticipant: socket.channel,
                    userToken: self.userToken
                });
            }
        }

        function onRemoteStreamStartsFlowing() {
            if(navigator.userAgent.match(/Android|iPhone|iPad|iPod|BlackBerry|IEMobile/i)) {
                // if mobile device
                return afterRemoteStreamStartedFlowing();
            }
            
            if (!(audio.readyState <= HTMLMediaElement.HAVE_CURRENT_DATA || audio.paused || audio.currentTime <= 0)) {
                afterRemoteStreamStartedFlowing();
            } else setTimeout(onRemoteStreamStartsFlowing, 50);
        }

        function sendsdp(sdp) {
            socket.send({
                userToken: self.userToken,
                sdp: JSON.stringify(sdp)
            });
        }

        function socketResponse(response) {
            if (response.userToken == self.userToken) return;
            if (response.sdp) {
                inner.sdp = JSON.parse(response.sdp);
                selfInvoker();
            }

            if (response.candidate && !gotstream) {
                if (!peer) console.error('missed an ice', response.candidate);
                else
                    peer.addICE({
                        sdpMLineIndex: response.candidate.sdpMLineIndex,
                        candidate: JSON.parse(response.candidate.candidate)
                    });
            }

            if (response.left) {
                if (peer && peer.peer) {
                    peer.peer.close();
                    peer.peer = null;
                }
            }
        }

        var invokedOnce = false;

        function selfInvoker() {
            if (invokedOnce) return;

            invokedOnce = true;

            if (isofferer) peer.addAnswerSDP(inner.sdp);
            else initPeer(inner.sdp);
        }
    }

    function leave() {
        var length = window.sockets.length;
        for (var i = 0; i < length; i++) {
            var socket = window.sockets[i];
            if (socket) {
                socket.send({
                    left: true,
                    userToken: self.userToken
                });
                delete window.sockets[i];
            }
        }

        // if owner leaves; try to remove his room from all other users side
        if (isbroadcaster) {
            defaultSocket.send({
                left: true,
                userToken: self.userToken,
                roomToken: self.roomToken
            });
        }

        if (config.attachStream) {
            if('stop' in config.attachStream) {
                config.attachStream.stop();
            }
            else {
                config.attachStream.getTracks().forEach(function(track) {
                    track.stop();
                });
            }
        }
    }
    
    window.addEventListener('beforeunload', function () {
        leave();
    }, false);

    window.addEventListener('keyup', function (e) {
        if (e.keyCode == 116)
            leave();
    }, false);

    function startBroadcasting() {
        defaultSocket && defaultSocket.send({
            roomToken: self.roomToken,
            roomName: self.roomName,
            broadcaster: self.userToken
        });
        setTimeout(startBroadcasting, 3000);
    }

    function onNewParticipant(channel) {
        if (!channel || channels.indexOf(channel) != -1 || channel == self.userToken) return;
        channels += channel + '--';

        var new_channel = uniqueToken();
        openSubSocket({
            channel: new_channel
        });

        defaultSocket.send({
            participant: true,
            userToken: self.userToken,
            joinUser: channel,
            channel: new_channel
        });
    }

    function uniqueToken() {
        var s4 = function() {
            return Math.floor(Math.random() * 0x10000).toString(16);
        };
        return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    }

    openDefaultSocket(config.onReady || function() {});

    return {
        createRoom: function(_config) {
            self.roomName = _config.roomName || 'Anonymous';
            self.roomToken = uniqueToken();

            isbroadcaster = true;
            isGetNewRoom = false;
            startBroadcasting();
        },
        joinRoom: function(_config) {
            self.roomToken = _config.roomToken;
            isGetNewRoom = false;

            self.joinedARoom = true;
            self.broadcasterid = _config.joinUser;

            openSubSocket({
                channel: self.userToken,
                callback: function() {
                    defaultSocket.send({
                        participant: true,
                        userToken: self.userToken,
                        joinUser: _config.joinUser
                    });
                }
            });
        },
        leaveRoom: leave
    };
};