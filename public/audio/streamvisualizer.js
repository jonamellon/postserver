/*
 * Copyright 2016 Boris Smus. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Adapted from Boris Smus's demo at http://webaudioapi.com/samples/visualizer

/* globals AudioContext, webkitAudioContext */

// constants defined for the whole program that cannot be reassigned
const WIDTH = 308;
const HEIGHT = 231;

// Interesting parameters to tweak!
//const SMOOTHING = 0.8;
//const FFT_SIZE = 2048;

// cope with browser differences
if (typeof AudioContext === 'function') {
    /// this one gets run
    ctx = new AudioContext;
} else if (typeof webkitAudioContext === 'function') {
    ctx = new webkitAudioContext(); // eslint-disable-line new-cap
} else {
    alert('Sorry! Web Audio is not supported by this browser');
}

function turnOnAudio() {
    ctx.resume();
}
function RemotePanner(remoteStream) {
    console.log('Creating StreamVisualizer with remoteStream: ',
        remoteStream);
    
    remoteStream.foo = "test";
    // Create a MediaStreamAudioSourceNode from the remoteStream
    remoteStream.source = ctx.createMediaStreamSource(remoteStream);
    console.log('Created Web Audio source from remote stream: ', remoteStream.source);
    
        

    //remoteStream.panner = ctx.createPanner();
    remoteStream.panner = new PannerNode(ctx, { panningModel: 'HRTF' });
    remoteStream.gain = ctx.createGain();
    remoteStream.filter = ctx.createBiquadFilter();
    remoteStream.filter.type = "highpass";
    remoteStream.filter.frequency.value = 80;
    remoteStream.filter.q = 0;

    //remoteStream.oscillator = ctx.createOscillator();
    //remoteStream.oscillator.type = 'square';
    //remoteStream.oscillator.frequency.setValueAtTime(220, ctx.currentTime); // value in hertz
    //remoteStream.oscillator.connect(remoteStream.panner);
    
    remoteStream.source.connect(remoteStream.panner);
       
    let x = 0;
    let y = 0;
    let z = 0;
    let mode = "inverse" 
    remoteStream.panner.maxDistance = 1 * mul;
    if (mode == "linear") {
        remoteStream.gain.gain.value = gainval;
        remoteStream.panner.distanceModel = "linear";
        remoteStream.panner.refDistance = 1 * mul;
        remoteStream.panner.rolloffFactor = rolloff;
        remoteStream.panner.coneInnerAngle = 360;
        remoteStream.panner.setPosition(x * mul, y * mul, z * mul);
        remoteStream.panner.panningModel = "HRTF";
    }
    if (mode == "exponential") {
        remoteStream.gain.gain.value = 30;
        remoteStream.panner.distanceModel = "exponential";
        remoteStream.panner.refDistance = 1 * mul;
        remoteStream.panner.rolloffFactor = rolloff;
        remoteStream.panner.coneInnerAngle = 360;
        remoteStream.panner.setPosition(x * mul, y * mul, z * mul);
        remoteStream.panner.panningModel = "HRTF";
    }
    if (mode == "inverse") {
        // mul = 10
        remoteStream.gain.gain.value = gainval;
        remoteStream.panner.distanceModel = "inverse";
        remoteStream.panner.refDistance = refdist * mul;
        remoteStream.panner.rolloffFactor = rolloff;
        remoteStream.panner.coneInnerAngle = 360;
        remoteStream.panner.setPosition(x * mul, y * mul, z * mul);
        remoteStream.panner.panningModel = "HRTF";
    }
    


    remoteStream.panner.connect(remoteStream.gain);
    remoteStream.startTime = 0;
    remoteStream.startOffset = 0;
    
    remoteStream.gain.connect(remoteStream.filter);
    //remoteStream.gain.connect(ctx.destination);
    remoteStream.filter.connect(ctx.destination);

    // last chance tests:
    //remoteStream.oscillator.start();
    //remoteStream.source.connect(ctx.destination);
}


