/*
 * Copyright 2016 Boris Smus. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Adapted from Boris Smus's demo at http://webaudioapi.com/samples/visualizer

/* globals AudioContext, webkitAudioContext */

// constants defined for the whole program that cannot be reassigned
const WIDTH = 308;
const HEIGHT = 231;

// Interesting parameters to tweak!
const SMOOTHING = 0.8;
const FFT_SIZE = 2048;

function StreamVisualizer(remoteStream, canvas) {
    console.log('Creating StreamVisualizer with remoteStream and canvas: ',
        remoteStream, canvas);
    this.canvas = canvas;
    this.drawContext = this.canvas.getContext('2d');

    // cope with browser differences
    if (typeof AudioContext === 'function') {
        /// this one gets run
        console.log("here1");
        this.context = new AudioContext();
    } else if (typeof webkitAudioContext === 'function') {
        console.log("here2");
        this.context = new webkitAudioContext(); // eslint-disable-line new-cap
    } else {
        alert('Sorry! Web Audio is not supported by this browser');
    }
    this.foo = "test";
    // Create a MediaStreamAudioSourceNode from the remoteStream
    this.source = this.context.createMediaStreamSource(remoteStream);
    console.log('Created Web Audio source from remote stream: ', this.source);
    
    //var source = context.createBufferSource();
    //passing in data
    //source.buffer = buffer;
    //giving the source which sound to play
    
  

    this.oscillator = this.context.createOscillator();
    this.oscillator.type = 'square';
    this.oscillator.frequency.setValueAtTime(220, this.context.currentTime); // value in hertz
        
    this.osgain = this.context.createGain();
    this.osgain.gain.value = 0.05;
    this.oscillator.connect(this.osgain);

    this.analyser = this.context.createAnalyser();
    this.panner= this.context.createPanner();
    //  this.analyser.connect(this.context.destination);
    this.analyser.minDecibels = -140;
    this.analyser.maxDecibels = 0;
    this.freqs = new Uint8Array(this.analyser.frequencyBinCount);
    this.times = new Uint8Array(this.analyser.frequencyBinCount);
    this.gain = this.context.createGain();
    this.gain.gain.value = 2;
    //source.connect(this.panner);
    this.source.connect(this.panner);
    this.osgain.connect(this.panner);
    this.oscillator.start();
    var x = -1.67;
    var y = -5;
    var z =  -3.74;
    this.panner.distanceModel = "exponential";
    this.panner.refDistance= 3 * mul;
    this.panner.rolloffFactor = 2;
    this.panner.coneInnerAngle = 360;
    this.panner.setPosition(x * mul, y * mul, z * mul);
    
    this.panner.connect(this.gain);
    // new line here:
    //this.source.connect(this.context.destination);
    this.gain.connect(this.context.destination);
    this.gain.connect(this.analyser);
    // window.AudioContext = this.AudioContext;
    // window.AudioContext.isPlaying = true;
    this.startTime = 0;
    this.startOffset = 0;
}

StreamVisualizer.prototype.update = function () {
    alert("Hello");
};

StreamVisualizer.prototype.start = function () {
    requestAnimationFrame(this.draw.bind(this));
};

StreamVisualizer.prototype.draw = function () {
    let barWidth;
    let offset;
    let height;
    let percent;
    let value;
    this.analyser.smoothingTimeConstant = SMOOTHING;
    this.analyser.fftSize = FFT_SIZE;

    // Get the frequency data from the currently playing music
    this.analyser.getByteFrequencyData(this.freqs);
    this.analyser.getByteTimeDomainData(this.times);


    this.canvas.width = WIDTH;
    this.canvas.height = HEIGHT;
    // Draw the frequency domain chart.
    for (let i = 0; i < this.analyser.frequencyBinCount; i++) {
        value = this.freqs[i];
        percent = value / 256;
        height = HEIGHT * percent;
        offset = HEIGHT - height - 1;
        barWidth = WIDTH / this.analyser.frequencyBinCount;
        let hue = i / this.analyser.frequencyBinCount * 360;
        this.drawContext.fillStyle = 'hsl(' + hue + ', 100%, 50%)';
        this.drawContext.fillRect(i * barWidth, offset, barWidth, height);
    }

    // Draw the time domain chart.
    for (let i = 0; i < this.analyser.frequencyBinCount; i++) {
        value = this.times[i];
        percent = value / 256;
        height = HEIGHT * percent;
        offset = HEIGHT - height - 1;
        barWidth = WIDTH / this.analyser.frequencyBinCount;
        this.drawContext.fillStyle = 'white';
        this.drawContext.fillRect(i * barWidth, offset, 1, 2);
    }

    requestAnimationFrame(this.draw.bind(this));
};

StreamVisualizer.prototype.getFrequencyValue = function (freq) {
    let nyquist = this.context.sampleRate / 2;
    let index = Math.round(freq / nyquist * this.freqs.length);
    return this.freqs[index];
};