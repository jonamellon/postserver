const express = require('express');
const multer = require('multer');
const upload = multer({dest: __dirname + '/public/uploads/images'});
var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey = fs.readFileSync('keys/private.key', 'utf8');
var certificate = fs.readFileSync('keys/certificate.crt', 'utf8');
var credentials = { key: privateKey, cert: certificate };

const app = express();
const PORT = 3000;

app.use(express.static('public'));

app.post('/upload', upload.single('photo'), (req, res) => {
    if(req.file) {
        res.json(req.file);
    }
    else throw 'error';
});

app.listen(PORT, () => {
    console.log('Listening at ' + PORT );
});

var httpServer = http.createServer(app);
var httpsServer = http.createServer(app);

httpServer.listen(8080);
httpsServer.listen(8443);